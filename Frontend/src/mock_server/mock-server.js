(function ($) {
    function MockServer() {
        var events = {
            "Total": 37
            , "Events": [{
                "Id": 1
                , "Name": "San Fernando Fall Harvest"
                , "Type": "festival"
                , "StartDate": "\/Date(1477083600000-0000)\/"
                , "EndDate": "\/Date(1477083600000-0000)\/"
                , "Region": "California"
                , "ImgUrl": null
            }, {
                "Id": 2
                , "Name": "Brewery Startup Camp in San Diego"
                , "Type": "other"
                , "StartDate": "\/Date(1470085200000-0000)\/"
                , "EndDate": "\/Date(1470776400000-0000)\/"
                , "Region": "CA"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/iiuM5Q/upload_jExMO1-large.png"
            }, {
                "Id": 3
                , "Name": "Craft Beer Connoisseur Camp in San Diego"
                , "Type": "other"
                , "StartDate": "\/Date(1469307600000-0000)\/"
                , "EndDate": "\/Date(1469998800000-0000)\/"
                , "Region": "California"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/6PBXvz/upload_2Kfngy-large.png"
            }, {
                "Id": 4
                , "Name": "Juan Carlos"
                , "Type": "festival"
                , "StartDate": "\/Date(1466802000000-0000)\/"
                , "EndDate": "\/Date(1466802000000-0000)\/"
                , "Region": "Illinois"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/L2dAEr/upload_CrFa7l-large.png"
            }, {
                "Id": 5
                , "Name": "Books and Beer"
                , "Type": "meetup"
                , "StartDate": "\/Date(1465938000000-0000)\/"
                , "EndDate": "\/Date(1465938000000-0000)\/"
                , "Region": "Illinois"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/xMLyOI/upload_AXWInT-large.png"
            }, {
                "Id": 6
                , "Name": "Freedom Fest Craft Beer Festival"
                , "Type": "festival"
                , "StartDate": "\/Date(1437166800000-0000)\/"
                , "EndDate": "\/Date(1437166800000-0000)\/"
                , "Region": "Connecticut"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/OUqh1N/upload_qoChhT-large.png"
            }, {
                "Id": 7
                , "Name": "Yellowstone Beer Fest"
                , "Type": "festival"
                , "StartDate": "\/Date(1437166800000-0000)\/"
                , "EndDate": "\/Date(1437166800000-0000)\/"
                , "Region": "Wyoming"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/0oZVAo/upload_KjVkrq-large.png"
            }, {
                "Id": 8
                , "Name": "Portland Craft Beer Festival"
                , "Type": "festival"
                , "StartDate": "\/Date(1435870800000-0000)\/"
                , "EndDate": "\/Date(1436043600000-0000)\/"
                , "Region": "OR"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/t5YwQy/upload_ILflrU-large.png"
            }, {
                "Id": 9
                , "Name": "Bare Beach Beer Bash"
                , "Type": "festival"
                , "StartDate": "\/Date(1435352400000-0000)\/"
                , "EndDate": "\/Date(1435352400000-0000)\/"
                , "Region": "Pennsylvania"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/mB7srw/upload_JObvzO-large.png"
            }, {
                "Id": 10
                , "Name": "Draft Line Brewing's One Year Anniversary Celebration"
                , "Type": "other"
                , "StartDate": "\/Date(1435352400000-0000)\/"
                , "EndDate": "\/Date(1435352400000-0000)\/"
                , "Region": "North Carolina"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/FS17Tg/upload_wXKRHV-large.png"
            }, {
                "Id": 11
                , "Name": "So Cal Brew Fest"
                , "Type": "festival"
                , "StartDate": "\/Date(1434747600000-0000)\/"
                , "EndDate": "\/Date(1434747600000-0000)\/"
                , "Region": "CA"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/7j0Yjq/upload_s3a8ti-large.png"
            }, {
                "Id": 12
                , "Name": "Little Elm Craft Brew & Que Festival"
                , "Type": "festival"
                , "StartDate": "\/Date(1434142800000-0000)\/"
                , "EndDate": "\/Date(1434142800000-0000)\/"
                , "Region": "Texas"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/9rP4iV/upload_oqSncg-large.png"
            }, {
                "Id": 13
                , "Name": "Grayslake Craft Beer Festival"
                , "Type": "festival"
                , "StartDate": "\/Date(1432933200000-0000)\/"
                , "EndDate": "\/Date(1432933200000-0000)\/"
                , "Region": "Illinois"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/vDxSPd/upload_xvbNZF-large.png"
            }, {
                "Id": 14
                , "Name": "Brew Fest on the Farm"
                , "Type": "festival"
                , "StartDate": "\/Date(1432933200000-0000)\/"
                , "EndDate": "\/Date(1432933200000-0000)\/"
                , "Region": "New York"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/XXgGZ4/upload_nqmfOW-large.png"
            }, {
                "Id": 15
                , "Name": "Hot Springs Craft Beer Festival"
                , "Type": "festival"
                , "StartDate": "\/Date(1432933200000-0000)\/"
                , "EndDate": "\/Date(1432933200000-0000)\/"
                , "Region": "Arkansas"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/d8IJKF/upload_BdaV3G-large.png"
            }, {
                "Id": 16
                , "Name": "No Boundaries on the River"
                , "Type": "festival"
                , "StartDate": "\/Date(1431723600000-0000)\/"
                , "EndDate": "\/Date(1431723600000-0000)\/"
                , "Region": "Washington"
                , "ImgUrl": null
            }, {
                "Id": 17
                , "Name": "Captain Lawrence Brewing Tap & Pour Celebration Party"
                , "Type": "other"
                , "StartDate": "\/Date(1431723600000-0000)\/"
                , "EndDate": "\/Date(1431723600000-0000)\/"
                , "Region": "New York"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/gaVq6l/upload_XaVOWk-large.png"
            }, {
                "Id": 18
                , "Name": "Canal Winchester BrewFest"
                , "Type": "festival"
                , "StartDate": "\/Date(1431723600000-0000)\/"
                , "EndDate": "\/Date(1431723600000-0000)\/"
                , "Region": "Ohio"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/IuEeGC/upload_F2n8L5-large.png"
            }, {
                "Id": 19
                , "Name": "Home Brew the Legal Way"
                , "Type": "seminar"
                , "StartDate": "\/Date(1431378000000-0000)\/"
                , "EndDate": "\/Date(1431378000000-0000)\/"
                , "Region": "California"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/MMSB2i/upload_E4Vo28-large.png"
            }, {
                "Id": 20
                , "Name": "Inaugural Indiana-Michigan Craft Beer Mayfest"
                , "Type": "festival"
                , "StartDate": "\/Date(1431118800000-0000)\/"
                , "EndDate": "\/Date(1431118800000-0000)\/"
                , "Region": "Indiana"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/pdLPeS/upload_YR7RYJ-large.png"
            }, {
                "Id": 21
                , "Name": "LA on Tap"
                , "Type": "festival_competition"
                , "StartDate": "\/Date(1431118800000-0000)\/"
                , "EndDate": "\/Date(1431118800000-0000)\/"
                , "Region": "California"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/4Gn1xK/upload_ITw0qe-large.png"
            }, {
                "Id": 22
                , "Name": "Thread City Hop Fest"
                , "Type": "festival"
                , "StartDate": "\/Date(1430600400000-0000)\/"
                , "EndDate": "\/Date(1430600400000-0000)\/"
                , "Region": "Connecticut"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/Rx4Dnt/upload_EBavLt-large.png"
            }, {
                "Id": 23
                , "Name": "6th Annual Beer Carnival"
                , "Type": "festival"
                , "StartDate": "\/Date(1426885200000-0000)\/"
                , "EndDate": "\/Date(1426885200000-0000)\/"
                , "Region": "Georgia"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/k2jMtH/upload_A3jnTD-large.png"
            }, {
                "Id": 24
                , "Name": "Legendary Weekend: Alamo Beer Company Brewery Grand Opening"
                , "Type": "other"
                , "StartDate": "\/Date(1425589200000-0000)\/"
                , "EndDate": "\/Date(1425762000000-0000)\/"
                , "Region": "TX"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/VPHkaP/upload_SxjOOL-large.png"
            }, {
                "Id": 25
                , "Name": "SF Beer week opening gala 2015"
                , "Type": "festival"
                , "StartDate": "\/Date(1423170000000-0000)\/"
                , "EndDate": "\/Date(1423170000000-0000)\/"
                , "Region": "California"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/Glq8HX/upload_5SxZEg-large.png"
            }, {
                "Id": 26
                , "Name": "SDBW Surfside Tap Room- Green Flash"
                , "Type": "tasting"
                , "StartDate": "\/Date(1415739600000-0000)\/"
                , "EndDate": "\/Date(1415739600000-0000)\/"
                , "Region": "California"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/eS7K86/upload_MUGmWU-large.png"
            }, {
                "Id": 27
                , "Name": "Untapped Fest Houston"
                , "Type": "festival"
                , "StartDate": "\/Date(1411160400000-0000)\/"
                , "EndDate": "\/Date(1411160400000-0000)\/"
                , "Region": "Texas"
                , "ImgUrl": null
            }, {
                "Id": 28
                , "Name": "Oktoberfest Houston"
                , "Type": "festival"
                , "StartDate": "\/Date(1410555600000-0000)\/"
                , "EndDate": "\/Date(1410555600000-0000)\/"
                , "Region": "Texas"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/BBE5WM/upload_1asFTP-large.png"
            }, {
                "Id": 29
                , "Name": "Oktoberfest Houston"
                , "Type": "festival"
                , "StartDate": "\/Date(1410555600000-0000)\/"
                , "EndDate": "\/Date(1410555600000-0000)\/"
                , "Region": "Texas"
                , "ImgUrl": null
            }, {
                "Id": 30
                , "Name": "B'dam BrewJAM"
                , "Type": "festival"
                , "StartDate": "\/Date(1410555600000-0000)\/"
                , "EndDate": "\/Date(1410555600000-0000)\/"
                , "Region": "Virginia"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/28OVyf/upload_yb7ZxA-large.png"
            }, {
                "Id": 31
                , "Name": "New England Brewfest"
                , "Type": "festival"
                , "StartDate": "\/Date(1403816400000-0000)\/"
                , "EndDate": "\/Date(1403989200000-0000)\/"
                , "Region": "New Hampshire"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/YrVHMv/upload_BqMFEA-large.png"
            }, {
                "Id": 32
                , "Name": "SAVOR"
                , "Type": "festival"
                , "StartDate": "\/Date(1339102800000-0000)\/"
                , "EndDate": "\/Date(1339189200000-0000)\/"
                , "Region": "District of Columbia"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/5ac2lB/upload_ZfiHfo-large.png"
            }, {
                "Id": 33
                , "Name": "American Craft Beer Fest (ACBF)"
                , "Type": "festival"
                , "StartDate": "\/Date(1338498000000-0000)\/"
                , "EndDate": "\/Date(1338584400000-0000)\/"
                , "Region": "Massachusetts"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/huKquc/upload_iykplk-large.png"
            }, {
                "Id": 34
                , "Name": "Copenhagen Beer Celebration"
                , "Type": "festival"
                , "StartDate": "\/Date(1336683600000-0000)\/"
                , "EndDate": "\/Date(1336770000000-0000)\/"
                , "Region": "København"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/wXmTDU/upload_gv9PhD-large.png"
            }, {
                "Id": 35
                , "Name": "New River Brewfest"
                , "Type": "festival"
                , "StartDate": "\/Date(1334350800000-0000)\/"
                , "EndDate": "\/Date(1334350800000-0000)\/"
                , "Region": "Virginia"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/KE3f43/upload_OSuuex-large.png"
            }, {
                "Id": 36
                , "Name": "Great American Beer Festival"
                , "Type": "festival_competition"
                , "StartDate": "\/Date(1317243600000-0000)\/"
                , "EndDate": "\/Date(1317416400000-0000)\/"
                , "Region": "Colorado"
                , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/TGtEbk/upload_Eh49UQ-large.png"
            }, {
                "Id": 37
                , "Name": "2008 Hickory Hops / Carolina Championship of "
                , "Type": "festival_competition"
                , "StartDate": "\/Date(1207947600000-0000)\/"
                , "EndDate": "\/Date(1207947600000-0000)\/"
                , "Region": "NC"
                , "ImgUrl": null
            }]
        };
        var oneEvent = {
            "Id": 3
            , "Name": "Craft Beer Connoisseur Camp in San Diego"
            , "Type": "other"
            , "StartDate": "\/Date(1469307600000-0000)\/"
            , "EndDate": "\/Date(1469998800000-0000)\/"
            , "Region": "California"
            , "Description": "The newest offering from San Diego State University's renowned Business of Craft Beer program: Eight days of intensive instruction by local craft-beer industry superstars, and one free day to explore San Diego. Ideal for those preparing for exams such as Cicerone® and BJCP, or for beer lovers who want to elevate their expertise. Check out the program video and the interview with interplanetary craft-beer ambassador Dr. Bill Sysak, an instructor with the program."
            , "Price": "$1,500"
            , "Website": "http://neverstoplearning.net/beercamp"
            , "Phone": "(619) 594-1138"
            , "Time": "9 full days"
            , "ImgUrl": "https://s3.amazonaws.com/brewerydbapi/event/6PBXvz/upload_2Kfngy-large.png"
        };
        var getFilteredEvents = function getFilteredEvents(name) {
            return events.Events.filter(function (e) {
                return name === "" || e.Name.toLowerCase().indexOf(name.toLowerCase()) !== -1;
            });
        };
        this.events = function (name) {
            return getFilteredEvents(name);
        };
        this.getEvent = function (id) {
            return oneEvent;
        };
        this.getRegions = function (name) {
            var sectorList = [{
                name:"California",
                eventsByRegion: 3
            }, {
                name:"Illinois",
                eventsByRegion: 6
            }, {
                name:"OR",
                eventsByRegion: 2
            }, {
                name:"CA",
                eventsByRegion: 5
            }, {
                name:"Texas",
                eventsByRegion: 1
            }];
            return sectorList;
        };
    };
    window.testSPA = window.testSPA || {};
    window.testSPA.mockServer = new MockServer();
})(jQuery);