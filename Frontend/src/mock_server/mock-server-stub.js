define(["jquery", "mock-server", "app/app.config", "jquery-mockjax"], function ($, mockServer, appConfig) {
    if (appConfig.isMockEnabled()) {
        $.mockjax({
            url: appConfig.getUrl() + '/Events.json'
            , responseTime: 30
            , response: function (settings) {
                this.responseText = mockServer.events(settings.data.name);
            }
        });
        $.mockjax({
            url: appConfig.getUrl() + '/Regions.json'
            , responseTime: 30
            , response: function (settings) {
                this.responseText = mockServer.getRegions(settings.data.name);
            }
        });

        $.mockjax({
            url: appConfig.getUrl() + '/Event.json'
            , responseTime: 30
            , response: function (settings) {
                this.responseText = mockServer.getEvent(parseInt(settings.data.id));
            }
        });
    }
});