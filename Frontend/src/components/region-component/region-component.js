define(['app/app.config', 'knockout', 'text!./region-component.html', 'extensions/charts'], function (appConfig, ko, templateMarkup) {

  function RegionComponent(params) {
      this.regions = ko.observableArray();

      var getRegions = function () {
          $.getJSON(appConfig.getUrl() + '/Regions.json', params.search.json(), this.regions);
      };

      getRegions.call(this);
      var subscription = params.search.json.subscribe(getRegions, this);
      this.dispose = function () { subscription.dispose(); };
  }

  return { viewModel: RegionComponent, template: templateMarkup };

});
