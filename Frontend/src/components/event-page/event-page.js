define(['app/app.config','knockout', 'text!./event-page.html', 'extensions/custom-format', 'extensions/charts'], function (appConfig, ko, templateMarkup) {

    function EventPage(route) {
        var self = this;
        this.Name = ko.observable('');
        this.Description = ko.observable('');
        this.Price = ko.observable('');
        this.Website = ko.observable('');
        this.ImgUrl = ko.observable('');
        

        $.getJSON(appConfig.getUrl() + '/Event.json', { id: route.id }, function (result) {
            self.Name(result.Name);
            self.Description(result.Description);
            self.Price(result.Price);
            self.Website(result.Website);
            self.ImgUrl(result.ImgUrl);
        }, this);

        this.chartOptions = {
            x: 'date',
            y: 'amount',
            classFn: function (d) {
                return d.valuation ? "valuation"
                    : d.amount > 0 ? "invested"
                    : "returned";
            }
        };

    }

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    EventPage.prototype.dispose = function() { };
  
    return { viewModel: EventPage, template: templateMarkup };

});
