define(["jquery", "knockout", "text!./events-component.html", "app/app.config", "extensions/custom-format"], function ($, ko, templateMarkup, appConfig) {
    function EventsComponentViewModel(params) {
        var self = this;
        this.events = ko.observableArray();
        var getEvents = function () {
            $.getJSON(appConfig.getUrl() + '/Events.json', params.search.json(), success);
        };

        function success(data) {
            self.events(data.map(toModel));
        }

        function toJsDate(json) {
            var re = /-?\d+/;
            var m = re.exec(json);
            return new Date(parseInt(m[0]));
        }

        function toModel(event) {
            var start = toJsDate(event.StartDate);
            var end = toJsDate(event.EndDate);
            return {
                Id: event.Id
                , Name: event.Name
                , Type: event.Type
                , StartDate: start
                , EndDate: end
                , Region: event.Region
                , ImgUrl: event.ImgUrl
            };
        }
        
        getEvents.call(this);
        var subscription = params.search.json.subscribe(getEvents, this);
        this.dispose = function () {
            subscription.dispose();
        };
        
        this.showEvent = function () {
            location.hash = 'event/' + this.id;
        };
    }
    return {
        viewModel: EventsComponentViewModel
        , template: templateMarkup
    };
});