define(['knockout', 'text!./events-filter.html'], function(ko, templateMarkup) {

  function EventsFilter(params) {
      this.search = params.search;
  }

  return { viewModel: EventsFilter, template: templateMarkup };

});
