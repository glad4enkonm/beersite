define(function () {
    
    var _url = "http://localhost:49917";
    var _mockEnabled = true;
    
    function _getUrl(){
        return _url;
    }
    
    function _isMockEnabled(){
        return _mockEnabled;
    }
    
    return{
        getUrl: _getUrl,
        isMockEnabled: _isMockEnabled
    }
});